<?php

namespace App\Http\Controllers;

use App\Flower;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Str;

class FlowerController extends Controller
{

    // 註冊
    public function store(Request $request)
    {

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email'],
            // 'email' => ['required', 'string', 'email', 'unique:flowers'],
            'password' => ['required', 'string', 'min:8','max:12'],
        ]);

        $api_token = Str::random(10);
        $Create = Flower::create([
            'name' =>$request['name'],
            'email' =>$request['email'],
            'password' => $request['password'],
            'api_token' => $api_token,
        ]);

        if ($Create)
            return "註冊成功...$api_token";

    }


// 查詢
    public function show()
    {
        return Auth::user();
    }


// 修改
    public function update(Request $request)
    {
        $request->validate([
            'name',
            'email' => 'unique:users|email',
            'password',
        ]);

        Auth::user()->update($request->all());

        echo  '資料修改成功，以下爲修改結果';
        return  $request->all();

    }


// 刪除
    public function destroy($api_token)
    {
        $flower = Flower::where('api_token',$api_token);
        if ($flower && $flower -> delete()){
            return 'Flower deleted successfully';
        }
        else{
            return '未成功刪除';
        }
    }
}

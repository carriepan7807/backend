<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/testa', function () {
    return 'Hello World';
});

Route::get('/testb/{id}', 'ExampleController@testb');

Route::post('/testc', 'ExampleController@testc');

/**
* 參考: https://ithelp.ithome.com.tw/articles/10226089
**/
// 註冊用
Route::post('/flower','FlowerController@store');

// 登入用
Route::post('/flower/login','FlowerLoginController@FlowerLogin');


//  預設 Flower 的 Middleware 驗證（透過 token ），讀取、修改、刪除、登出
//  有兩種方式，擇一使用，建議使用 方式一。

// 方式一：驗證群組化

Route::group(['middleware' => ['auth:api']], function(){
   Route::get('/flower', 'FlowerController@show');
   Route::put('/flower', 'FlowerController@update');
   Route::delete('/flower/{api_token}', 'FlowerController@destroy');
   // Route::get('/flower','FlowerLogoutController@FlowerLogout');
});

